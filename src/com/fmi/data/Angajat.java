package com.fmi.data;

import java.util.List;

public class Angajat extends APersoana {
	private String id;
	private int salariu;
	private List<String> proiecte;
	private List<String> resurse;
	public Angajat(String cnp, String nume, String prenume, String adresa, Boolean sex, String id, int salariu,
			List<String> proiecte, List<String> resurse) {
		super(cnp, nume, prenume, adresa, sex);
		this.id = id;
		this.salariu = salariu;
		this.proiecte = proiecte;
		this.resurse = resurse;
	}
	public String getId() {
		return id;
	}
	private void setId(String id) {
		this.id = id;
	}
	public int getSalariu() {
		return salariu;
	}
	public void setSalariu(int salariu) {
		this.salariu = salariu;
	}
	public List<String> getProiecte() {
		return proiecte;
	}
	public void setProiecte(List<String> proiecte) {
		this.proiecte = proiecte;
	}
	public void addProiect(String proiect) {
		this.proiecte.add(proiect);
	}
	public void addProiecte(List<String> proiecte) {
		this.proiecte.addAll(proiecte);
	}
	public void removeProiect(String proiect) {
		this.proiecte.remove(this.proiecte.stream().filter(e -> e.equalsIgnoreCase(proiect)).findFirst().get());
	}
	public void removeProiecte(List<String> proiecte) {
		proiecte.forEach(f -> this.proiecte.remove(this.proiecte.stream().filter(e -> e.equalsIgnoreCase(f)).findFirst().get()));
	}
	public List<String> getResurse() {
		return resurse;
	}
	public void setResurse(List<String> resurse) {
		this.resurse = resurse;
	}
	public void addResursa(String resursa) {
		this.resurse.add(resursa);
	}
	public void addResurse(List<String> resurse) {
		this.resurse.addAll(resurse);
	}
	public void removeResursa(String resursa) {
		this.resurse.remove(this.resurse.stream().filter(e -> e.equalsIgnoreCase(resursa)).findFirst().get());
	}
	public void removeResurse(List<String> resurse) {
		resurse.forEach(f -> this.resurse.remove(this.resurse.stream().filter(e -> e.equalsIgnoreCase(f)).findFirst().get()));
	}
}
