package com.fmi.data;

public class Pc extends AResursa {
	private String adresaIp;
	private String sistemOperare;
	public Pc(String id, double valoareLichida, String adresaIp, String sistemOperare) {
		super(id, valoareLichida);
		this.adresaIp = adresaIp;
		this.sistemOperare = sistemOperare;
	}
	public String getAdresaIp() {
		return adresaIp;
	}
	public void setAdresaIp(String adresaIp) {
		this.adresaIp = adresaIp;
	}
	public String getSistemOperare() {
		return sistemOperare;
	}
	public void setSistemOperare(String sistemOperare) {
		this.sistemOperare = sistemOperare;
	}
}
