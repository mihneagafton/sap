package com.fmi.data;

public class Masina extends AResursa {
	private String culoare;
	private String anFabricatie;
	private String marca;
	private String motorizare;
	private String numarInmatriculare;
	public Masina(String id, double valoareLichida, String culoare, String anFabricatie, String marca,
			String motorizare, String numarInmatriculare) {
		super(id, valoareLichida);
		this.culoare = culoare;
		this.anFabricatie = anFabricatie;
		this.marca = marca;
		this.motorizare = motorizare;
		this.numarInmatriculare = numarInmatriculare;
	}
	public String getCuloare() {
		return culoare;
	}
	public void setCuloare(String culoare) {
		this.culoare = culoare;
	}
	public String getAnFabricatie() {
		return anFabricatie;
	}
	public void setAnFabricatie(String anFabricatie) {
		this.anFabricatie = anFabricatie;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getMotorizare() {
		return motorizare;
	}
	public void setMotorizare(String motorizare) {
		this.motorizare = motorizare;
	}
	public String getNumarInmatriculare() {
		return numarInmatriculare;
	}
	public void setNumarInmatriculare(String numarInmatriculare) {
		this.numarInmatriculare = numarInmatriculare;
	}
	
}
