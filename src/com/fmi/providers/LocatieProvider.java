package com.fmi.providers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import com.fmi.data.Locatie;
import com.fmi.services.LocatieService;

public class LocatieProvider {

	public void populate(String file) {
		try (
	            Reader reader = Files.newBufferedReader(Paths.get(file));
	            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
	        )
		{
			for(CSVRecord csvRecord : csvParser)
			{
				String adresaLocatie = csvRecord.get(0);
				String angajatiLocatieStr = csvRecord.get(1);
				List<String> angajatiLocatie = Arrays.asList(angajatiLocatieStr.split(";"));
				short capacitateLocatie = (short)Integer.parseInt(csvRecord.get(2));
				String idLocatie = csvRecord.get(3);
				short locuriParcareLocatie = (short)Integer.parseInt(csvRecord.get(4));
				Locatie locatie = new Locatie(idLocatie,adresaLocatie, capacitateLocatie, locuriParcareLocatie, angajatiLocatie);
				LocatieService.getInstance().add(locatie);
			}
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public void save(String file) {
	    try (
	    		FileWriter out = new FileWriter(file);
	    		CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT)) {
	    	LocatieService.getInstance().list().forEach(id -> {
	    		Locatie locatie = LocatieService.getInstance().get(id);
	    		StringBuilder angajati = new StringBuilder();
	    		locatie.getAngajati().forEach(angajat -> angajati.append(angajat).append(";"));
	    		angajati.deleteCharAt(angajati.length()-1);
	    		try {
					printer.printRecord(locatie.getAdresa(), angajati.toString(), locatie.getCapacitate(), locatie.getId(), locatie.getLocuriParcare());
				} catch (IOException e) {
					e.printStackTrace();
				}
	    	});
	    }
	    catch(Exception e) {
	    	System.out.println("AngajatProvider " + e.getMessage());
	    }
	}

}
