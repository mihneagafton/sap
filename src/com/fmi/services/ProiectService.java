package com.fmi.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fmi.data.Proiect;

public class ProiectService implements IService<Proiect> {
	private Map<String, Proiect> proiecte;
	private static ProiectService instance;
	
	private ProiectService() {
		super();
		this.proiecte = new HashMap<>();
	}
	public static ProiectService getInstance() {
		return (instance == null) ? new ProiectService() : instance;
	}
	
	
	@Override
	public Proiect get(String id) {
		return proiecte.get(id);
	}
	@Override
	public void remove(String id) {
		proiecte.remove(id);
	}
	@Override
	public void remove(List<String> ids) {
		ids.forEach(e -> proiecte.remove(e));
	}
	@Override
	public void add(Proiect object) {
		proiecte.put(object.getId(), object);
	}
	@Override
	public void add(List<Proiect> objects) {
		objects.forEach(e -> proiecte.put(e.getId(), e));
	}
	
	
	
	public String getNume(String id) {
		return get(id).getNume();
	}
	public void setNume(String id, String nume) {
		get(id).setNume(nume);
	}
	public String getDescriere(String id) {
		return get(id).getDescriere();
	}
	public void setDescriere(String id, String descriere) {
		get(id).setDescriere(descriere);
	}
	public List<String> getAngajati(String id) {
		return get(id).getAngajati();
	}
	public void setAngajati(String id, List<String> angajati) {
		get(id).setAngajati(angajati);
	}
	public void addAngajat(String id, String angajat) {
		get(id).addAngajat(angajat);
	}
	public void addAngajati(String id, List<String> angajati) {
		get(id).addAngajati(angajati);
	}
	public void removeAngajat(String id, String angajat) {
		get(id).removeAngajat(angajat);
	}
	public void removeAngajati(String id, List<String> angajati) {
		get(id).removeAngajati(angajati);
	}
	public List<String> getResurse(String id) {
		return get(id).getResurse();
	}
	public void setResurse(String id, List<String> resurse) {
		get(id).setResurse(resurse);
	}
	public void addResursa(String id, String resursa) {
		get(id).addResursa(resursa);
	}
	public void addResurse(String id, List<String> resurse) {
		get(id).addResurse(resurse);
	}
	public void removeResursa(String id, String resursa) {
		get(id).removeResursa(resursa);
	}
	public void removeResurse(String id, List<String> resurse) {
		get(id).removeResurse(resurse);
	}
	@Override
	public List<String> list() {
		return proiecte.keySet().stream().collect(Collectors.toList());
	}
	
}
