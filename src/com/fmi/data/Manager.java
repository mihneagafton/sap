package com.fmi.data;

import java.util.List;

public class Manager {
	private String selfId; // id-ul lui ca angajat
	private List<String> proiecte; // proiecte gestionate de el
	public Manager(String selfId, List<String> proiecte) {
		super();
		this.selfId = selfId;
		this.proiecte = proiecte;
	}
	public String getSelfId() {
		return selfId;
	}
	public void setSelfId(String selfId) {
		this.selfId = selfId;
	}
	public List<String> getProiecte() {
		return proiecte;
	}
	public void setProiecte(List<String> proiecte) {
		this.proiecte = proiecte;
	}
	public void addProiect(String proiect) {
		this.proiecte.add(proiect);
	}
	public void addProiecte(List<String> proiecte) {
		this.proiecte.addAll(proiecte);
	}
	public void removeProiect(String proiect) {
		this.proiecte.remove(this.proiecte.stream().filter(e -> e.equalsIgnoreCase(proiect)).findFirst().get());
	}
	public void removeProiecte(List<String> proiecte) {
		proiecte.forEach(f -> this.proiecte.remove(this.proiecte.stream().filter(e -> e.equalsIgnoreCase(f)).findFirst().get()));
	}
}
