package com.fmi.providers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import com.fmi.data.Angajat;
import com.fmi.data.AngajatEngineer;
import com.fmi.data.AngajatHr;
import com.fmi.data.AngajatIt;
import com.fmi.services.AngajatService;

public class AngajatProvider {

	public void populate(String file) {
		try (
	            Reader reader = Files.newBufferedReader(Paths.get(file));
	            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
	        )
		{
			for(CSVRecord csvRecord : csvParser)
			{
				String tipAngajat = csvRecord.get(0);
				String idAngajat = csvRecord.get(1);
				String numeAngajat = csvRecord.get(2);
				String prenumeAngajat = csvRecord.get(3);
				String cnpAngajat = csvRecord.get(4);
				String adresaAngajat = csvRecord.get(5);
				Boolean sexAngajat = csvRecord.get(6).equalsIgnoreCase("true");
				int salariuAngajat = Integer.parseInt(csvRecord.get(7));
				String proiecteAngajatStr = csvRecord.get(8);
				List<String> proiecteAngajat = Arrays.asList(proiecteAngajatStr.split(";"));
				String resurseAngajatStr = csvRecord.get(9);
				List<String> resurseAngajat = Arrays.asList(resurseAngajatStr.split(";"));
				Angajat angajat = null;
				switch(tipAngajat) {
				case "Hr": {
					angajat = new AngajatHr(cnpAngajat, numeAngajat, prenumeAngajat, adresaAngajat, sexAngajat, idAngajat, salariuAngajat, proiecteAngajat, resurseAngajat);
					break;
				}
				case "Engineer": {
					angajat = new AngajatEngineer(cnpAngajat, numeAngajat, prenumeAngajat, adresaAngajat, sexAngajat, idAngajat, salariuAngajat, proiecteAngajat, resurseAngajat);
					break;
				}
				case "It": {
					angajat = new AngajatIt(cnpAngajat, numeAngajat, prenumeAngajat, adresaAngajat, sexAngajat, idAngajat, salariuAngajat, proiecteAngajat, resurseAngajat);
					break;
				}
				default : {
					System.out.println("Eroare in csvAngajat");
					break;
				}
				}
				AngajatService.getInstance().add(angajat);
			}
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void save(String file) {
	    try (
	    		FileWriter out = new FileWriter(file);
	    		CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT)) {
	    	AngajatService.getInstance().list().forEach(id -> {
	    		Angajat angajat = AngajatService.getInstance().get(id);
	    		String tipAngajat = "";
	    		if(angajat instanceof AngajatHr) tipAngajat += "Hr";
	    		if(angajat instanceof AngajatEngineer) tipAngajat += "Engineer";
	    		if(angajat instanceof AngajatIt) tipAngajat += "It";
	    		StringBuilder proiecte = new StringBuilder();
	    		angajat.getProiecte().forEach(proiect -> proiecte.append(proiect).append(";"));
	    		proiecte.deleteCharAt(proiecte.length()-1);
	    		StringBuilder resurse = new StringBuilder();
	    		angajat.getResurse().forEach(resursa -> resurse.append(resursa).append(";"));
	    		resurse.deleteCharAt(resurse.length()-1);
	    		try {
					printer.printRecord(tipAngajat, angajat.getId(), angajat.getNume(), angajat.getPrenume(), angajat.getCnp(), angajat.getAdresa(), angajat.getSex(), angajat.getSalariu(), proiecte.toString(), resurse.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
	    	});
	    }
	    catch(Exception e) {
	    	System.out.println("AngajatProvider " + e.getMessage());
	    }
	}
}
