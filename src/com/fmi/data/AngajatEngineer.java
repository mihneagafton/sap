package com.fmi.data;

import java.util.List;

public class AngajatEngineer extends Angajat {

	public AngajatEngineer(String cnp, String nume, String prenume, String adresa, Boolean sex, String id,
			int salariu, List<String> proiecte, List<String> resurse) {
		super(cnp, nume, prenume, adresa, sex, id, salariu, proiecte, resurse);
	}

}
