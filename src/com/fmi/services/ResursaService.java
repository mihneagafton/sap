package com.fmi.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fmi.data.AResursa;
import com.fmi.data.Masina;
import com.fmi.data.Pc;

public class ResursaService implements IService<AResursa> {
	private Map<String, AResursa> resurse;
	private static ResursaService instance;
	
	private ResursaService() {
		super();
		this.resurse = new HashMap<>();
	}
	public static ResursaService getInstance() {
		return (instance == null) ? new ResursaService() : instance;
	}
	@Override
	public AResursa get(String id) {
		return resurse.get(id);
	}
	@Override
	public void remove(String id) {
		resurse.remove(id);
	}
	@Override
	public void remove(List<String> ids) {
		ids.forEach(e -> resurse.remove(e));
	}
	@Override
	public void add(AResursa object) {
		resurse.put(object.getId(), object);
	}
	@Override
	public void add(List<AResursa> objects) {
		objects.forEach(e -> resurse.put(e.getId(), e));
	}
	
	public boolean isMasina(String id) {
		return get(id) instanceof Masina;
	}
	public boolean isPc(String id) {
		return get(id) instanceof Pc;
	}
	
	public double getValoareLichida(String id) {
		return get(id).getValoareLichida();
	}
	public void setValoareLichida(String id, double valoareLichida) {
		get(id).setValoareLichida(valoareLichida);
	}
	@Override
	public List<String> list() {
		return resurse.keySet().stream().collect(Collectors.toList());
	}
}
