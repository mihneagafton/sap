package com.fmi.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fmi.data.Angajat;
import com.fmi.data.AngajatEngineer;
import com.fmi.data.AngajatHr;
import com.fmi.data.AngajatIt;

public class AngajatService implements IService<Angajat>{
	private Map<String, Angajat> angajati;
	private static AngajatService instance;

	private AngajatService() {
		super();
		this.angajati = new HashMap<>();
	}
	public static AngajatService getInstance() {
		return (instance == null) ? new AngajatService() : instance;
	}

	@Override
	public Angajat get(String id) {
		return angajati.get(id);
	}

	@Override
	public void remove(String id) {
		angajati.remove(id);
	}

	@Override
	public void remove(List<String> ids) {
		ids.forEach(e -> angajati.remove(e));
		
	}

	@Override
	public void add(Angajat object) {
		angajati.put(object.getId(), object);
	}

	@Override
	public void add(List<Angajat> objects) {
		objects.forEach(e -> angajati.put(e.getId(), e));
	}
	
	
	public boolean isEngineer(String id) {
		return get(id) instanceof AngajatEngineer;
	}
	public boolean isHr(String id) {
		return get(id) instanceof AngajatHr;
	}
	public boolean isIt(String id) {
		return get(id) instanceof AngajatIt;
	}
	public boolean isEmployed(String id) {
		return (get(id) != null);
	}
	
	
	public int getSalariu(String id) {
		return get(id).getSalariu();
	}
	public void setSalariu(String id, int salariu) {
		get(id).setSalariu(salariu);
	}
	public List<String> getProiecte(String id) {
		return get(id).getProiecte();
	}
	public void setProiecte(String id, List<String> proiecte) {
		get(id).setProiecte(proiecte);
	}
	public void addProiect(String id, String proiect) {
		get(id).addProiect(proiect);
	}
	public void addProiecte(String id, List<String> proiecte) {
		get(id).addProiecte(proiecte);
	}
	public void removeProiect(String id, String proiect) {
		get(id).removeProiect(proiect);
	}
	public void removeProiecte(String id, List<String> proiecte) {
		get(id).removeProiecte(proiecte);
	}
	public List<String> getResurse(String id) {
		return get(id).getResurse();
	}
	public void setResurse(String id, List<String> resurse) {
		get(id).setResurse(resurse);
	}
	public void addResursa(String id, String resursa) {
		get(id).addResursa(resursa);
	}
	public void addResurse(String id, List<String> resurse) {
		get(id).addResurse(resurse);
	}
	public void removeResursa(String id, String resursa) {
		get(id).removeResursa(resursa);
	}
	public void removeResurse(String id, List<String> resurse) {
		get(id).removeResurse(resurse);
	}
	
	 // TODO Ar trebuie sa fac proxy si la geterele si seterele  din APersoana ????
	
	public String getCnp(String id) {
		return get(id).getCnp();
	}
	public void setCnp(String id, String cnp) {
		get(id).setCnp(cnp);
	}
	public String getNume(String id) {
		return get(id).getNume();
	}
	public void setNume(String id, String nume) {
		get(id).setNume(nume);
	}
	public String getPrenume(String id) {
		return get(id).getPrenume();
	}
	public void setPrenume(String id, String prenume) {
		get(id).setPrenume(prenume);
	}
	public String getAdresa(String id) {
		return get(id).getAdresa();
	}
	public void setAdresa(String id, String adresa) {
		get(id).setAdresa(adresa);
	}
	public Boolean getSex(String id) {
		return get(id).getSex();
	}
	public void setSex(String id, Boolean sex) {
		get(id).setSex(sex);
	}
	@Override
	public List<String> list() {
		return angajati.keySet().stream().collect(Collectors.toList());
	}
}
