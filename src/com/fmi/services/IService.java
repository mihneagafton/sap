package com.fmi.services;

import java.util.List;

public interface IService <T> {
	public T get(String id);
	public void remove(String id);
	public void remove(List<String> ids);
	public void add(T object);
	public void add(List<T> objects);
	public List<String> list();
}
