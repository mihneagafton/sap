package com.fmi.data;

import java.util.List;

public class Locatie {
	private String id;
	private String adresa;
	private short capacitate;
	private short locuriParcare;
	private List<String> angajati;
	public Locatie(String id, String adresa, short capacitate, short locuriParcare, List<String> angajati) {
		super();
		this.id = id;
		this.adresa = adresa;
		this.capacitate = capacitate;
		this.locuriParcare = locuriParcare;
		this.angajati = angajati;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public short getCapacitate() {
		return capacitate;
	}
	public void setCapacitate(short capacitate) {
		this.capacitate = capacitate;
	}
	public short getLocuriParcare() {
		return locuriParcare;
	}
	public void setLocuriParcare(short locuriParcare) {
		this.locuriParcare = locuriParcare;
	}
	public List<String> getAngajati() {
		return angajati;
	}
	public void setAngajati(List<String> angajati) {
		this.angajati = angajati;
	}
	public void addAngajat(String angajat) {
		this.angajati.add(angajat);
	}
	public void addAngajati(List<String> angajati) {
		this.angajati.addAll(angajati);
	}
	public void removeAngajat(String angajat) {
		this.angajati.remove(this.angajati.stream().filter(e -> e.equalsIgnoreCase(angajat)).findFirst().get());
	}
	public void removeAngajati(List<String> angajati) {
		angajati.forEach(f -> this.angajati.remove(this.angajati.stream().filter(e -> e.equalsIgnoreCase(f)).findFirst().get()));
	}
}
