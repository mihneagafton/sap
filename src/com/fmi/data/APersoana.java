package com.fmi.data;

public abstract class APersoana {
	private String cnp;
	private String nume;
	private String prenume;
	private String adresa;
	private Boolean sex; // M - 0 ; F - 1
	
	public APersoana(String cnp, String nume, String prenume, String adresa, Boolean sex) {
		super();
		this.cnp = cnp;
		this.nume = nume;
		this.prenume = prenume;
		this.adresa = adresa;
		this.sex = sex;
	}
	
	public String getCnp() {
		return cnp;
	}
	public void setCnp(String cnp) {
		this.cnp = cnp;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public Boolean getSex() {
		return sex;
	}
	public void setSex(Boolean sex) {
		this.sex = sex;
	}
}
