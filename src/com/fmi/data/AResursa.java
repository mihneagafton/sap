package com.fmi.data;

public abstract class AResursa {
	private String id;
	private double valoareLichida;
	public AResursa(String id, double valoareLichida) {
		super();
		this.id = id;
		this.valoareLichida = valoareLichida;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getValoareLichida() {
		return valoareLichida;
	}
	public void setValoareLichida(double valoareLichida) {
		this.valoareLichida = valoareLichida;
	}
}
