package com.fmi.data;

import java.util.List;

public class Proiect {
	private String id;
	private String nume;
	private String descriere;
	private List<String> angajati;
	private List<String> resurse;
	public Proiect(String id, String nume, String descriere, List<String> angajati, List<String> resurse) {
		super();
		this.id = id;
		this.nume = nume;
		this.descriere = descriere;
		this.angajati = angajati;
		this.resurse = resurse;
	}
	public String getId() {
		return id;
	}
	private void setId(String id) {
		this.id = id;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getDescriere() {
		return descriere;
	}
	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}
	public List<String> getAngajati() {
		return angajati;
	}
	public void setAngajati(List<String> angajati) {
		this.angajati = angajati;
	}
	public void addAngajat(String angajat) {
		this.angajati.add(angajat);
	}
	public void addAngajati(List<String> angajati) {
		this.angajati.addAll(angajati);
	}
	public void removeAngajat(String angajat) {
		this.angajati.remove(this.angajati.stream().filter(e -> e.equalsIgnoreCase(angajat)).findFirst().get());
	}
	public void removeAngajati(List<String> angajati) {
		angajati.forEach(f -> this.angajati.remove(this.angajati.stream().filter(e -> e.equalsIgnoreCase(f)).findFirst().get()));
	}
	public List<String> getResurse() {
		return resurse;
	}
	public void setResurse(List<String> resurse) {
		this.resurse = resurse;
	}
	public void addResursa(String resursa) {
		this.resurse.add(resursa);
	}
	public void addResurse(List<String> resurse) {
		this.resurse.addAll(resurse);
	}
	public void removeResursa(String resursa) {
		this.resurse.remove(this.resurse.stream().filter(e -> e.equalsIgnoreCase(resursa)).findFirst().get());
	}
	public void removeResurse(List<String> resurse) {
		resurse.forEach(f -> this.resurse.remove(this.resurse.stream().filter(e -> e.equalsIgnoreCase(f)).findFirst().get()));
	}
	
}
