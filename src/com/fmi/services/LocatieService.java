package com.fmi.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fmi.data.Locatie;

public class LocatieService implements IService<Locatie> {
	private Map<String, Locatie> locatii;
	private static LocatieService instance;
	
	private LocatieService() {
		super();
		this.locatii = new HashMap<>();
	}
	public static LocatieService getInstance() {
		return (instance == null) ? new LocatieService() : instance;
	}
	@Override
	public Locatie get(String id) {
		return locatii.get(id);
	}
	@Override
	public void remove(String id) {
		locatii.remove(id);
	}
	@Override
	public void remove(List<String> ids) {
		ids.forEach(e -> locatii.remove(e));
	}
	@Override
	public void add(Locatie object) {
		locatii.put(object.getId(), object);
	}
	@Override
	public void add(List<Locatie> objects) {
		objects.forEach(e -> locatii.put(e.getId(), e));
	}
	
	public String getAdresa(String id) {
		return get(id).getAdresa();
	}
	public void setAdresa(String id, String adresa) {
		get(id).setAdresa(adresa);
	}
	public short getCapacitate(String id) {
		return get(id).getCapacitate();
	}
	public void setCapacitate(String id, short capacitate) {
		get(id).setCapacitate(capacitate);
	}
	public short getLocuriParcare(String id) {
		return get(id).getLocuriParcare();
	}
	public void setLocuriParcare(String id, short locuriParcare) {
		get(id).setLocuriParcare(locuriParcare);
	}
	public List<String> getAngajati(String id) {
		return get(id).getAngajati();
	}
	public void setAngajati(String id, List<String> angajati) {
		get(id).setAngajati(angajati);
	}
	public void addAngajat(String id, String angajat) {
		get(id).addAngajat(angajat);
	}
	public void addAngajati(String id, List<String> angajati) {
		get(id).addAngajati(angajati);
	}
	public void removeAngajat(String id, String angajat) {
		get(id).removeAngajat(angajat);
	}
	public void removeAngajati(String id, List<String> angajati) {
		get(id).removeAngajati(angajati);
	}
	@Override
	public List<String> list() {
		return locatii.keySet().stream().collect(Collectors.toList());
	}
	
	
}
